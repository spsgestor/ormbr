{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)
  @abstract(Website : http://www.ormbr.com.br)
  @abstract(Telagram : https://t.me/ormbr)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.objects.helper;

interface

uses
  DB,
  Rtti,
  Variants,
  SysUtils,
  Generics.Collections,
  ormbr.rtti.helper,
  ormbr.mapping.explorer,
  ormbr.mapping.classes,
  ormbr.mapping.attributes;

type
  TORMBrObject = class
  public
    constructor Create; virtual;
  end;

  TObjectHelper = class helper for TObject
  public
    function GetTable: Table;
    function GetResource: Resource;
    function GetNotServerUse: NotServerUse;
    function GetSubResource: SubResource;
    function &GetType(out AType: TRttiType): Boolean;
    function GetSequence: Sequence;
    function GetPrimaryKey: TArray<TColumnMapping>;
    function GetColumns: TArray<TRttiProperty>;
    function MethodCall(AMethodName: string;
      const AParameters: array of TValue): TValue;
    procedure SetDefaultValue;
  end;

implementation

var
  Context: TRttiContext;

{ TObjectHelper }

function TObjectHelper.GetColumns: TArray<TRttiProperty>;
var
  LType: TRttiType;
  LProperty: TRttiProperty;
  LAttribute: TCustomAttribute;
  LLength: Integer;
begin
   LLength := -1;
   if &GetType(LType) then
   begin
      for LProperty in LType.GetProperties do
      begin
         for LAttribute in LProperty.GetAttributes do
         begin
            if (LAttribute is Column) then // Column
            begin
              Inc(LLength);
              SetLength(Result, LLength +1);
              Result[LLength] := LProperty;
            end;
         end;
      end;
   end;
end;

function TObjectHelper.GetNotServerUse: NotServerUse;
var
  LType: TRttiType;
  LAttribute: TCustomAttribute;
begin
  Result := nil;
  if &GetType(LType) then
  begin
    for LAttribute in LType.GetAttributes do // NotServerUse
    begin
      if LAttribute is NotServerUse then
        Exit(NotServerUse(LAttribute));
    end;
    Exit(nil);
  end;
end;

function TObjectHelper.GetPrimaryKey: TArray<TColumnMapping>;
var
  LCols: Integer;
  LPkList: TList<TColumnMapping>;
  LColumns: TColumnMappingList;
  LColumn: TColumnMapping;
begin
  LPkList := TList<TColumnMapping>.Create;
  try
    LColumns := TMappingExplorer.GetInstance.GetMappingColumn(Self.ClassType);
    for LColumn in LColumns do
      if LColumn.IsPrimaryKey then
        LPkList.Add(LColumn);
    ///
    if LPkList.Count > 0 then
    begin
      SetLength(Result, LPkList.Count);
      for LCols := 0 to LPkList.Count -1 do
        Result[LCols] := LPkList.Items[LCols];
    end
    else
      Exit(nil);
  finally
    LPkList.Free;
  end;
end;

function TObjectHelper.GetResource: Resource;
var
  LType: TRttiType;
  LAttribute: TCustomAttribute;
begin
  Result := nil;
  if &GetType(LType) then
  begin
    for LAttribute in LType.GetAttributes do // Resource
    begin
      if LAttribute is Resource then
        Exit(Resource(LAttribute));
    end;
    Exit(nil);
  end;
end;

function TObjectHelper.GetSequence: Sequence;
var
  LType: TRttiType;
  LAttribute: TCustomAttribute;
begin
  Result := nil;
  if &GetType(LType) then
  begin
    for LAttribute in LType.GetAttributes do
    begin
      if LAttribute is Sequence then // Sequence
        Exit(Sequence(LAttribute));
    end;
  end;
end;

function TObjectHelper.GetSubResource: SubResource;
var
  LType: TRttiType;
  LAttribute: TCustomAttribute;
begin
  if &GetType(LType) then
  begin
    for LAttribute in LType.GetAttributes do // SubResource
    begin
      if LAttribute is SubResource then
        Exit(SubResource(LAttribute));
    end;
    Exit(nil);
  end
  else
    Exit(nil);
end;

function TObjectHelper.GetTable: Table;
var
  LType: TRttiType;
  LAttribute: TCustomAttribute;
begin
  if &GetType(LType) then
  begin
    for LAttribute in LType.GetAttributes do
    begin
      if (LAttribute is Table) or (LAttribute is View) then // Table/View
        Exit(Table(LAttribute));
    end;
    Exit(nil);
  end
  else
    Exit(nil);
end;

function TObjectHelper.&GetType(out AType: TRttiType): Boolean;
begin
  Result := False;
  if Assigned(Self) then
  begin
    AType  := Context.GetType(Self.ClassType);
    Result := Assigned(AType);
  end;
end;

function TObjectHelper.MethodCall(AMethodName: string;
  const AParameters: array of TValue): TValue;
var
  LRttiType: TRttiType;
  LMethod: TRttiMethod;
begin
  LRttiType := Context.GetType(Self.ClassType);
  LMethod   := LRttiType.GetMethod(AMethodName);
  if Assigned(LMethod) then
     Result := LMethod.Invoke(Self, AParameters)
  else
     raise Exception.CreateFmt('Cannot find method "%s" in the object', [AMethodName]);
end;

procedure TObjectHelper.SetDefaultValue;
var
  LColumns: TColumnMappingList;
  LColumn: TColumnMapping;
  LProperty: TRttiProperty;
  LValue: Variant;
begin
  LColumns := TMappingExplorer
                .GetInstance
                  .GetMappingColumn(Self.ClassType);
  for LColumn in LColumns do
  begin
    LProperty := LColumn.PropertyRtti;
    LValue := StringReplace(LColumn.DefaultValue, '''', '', [rfReplaceAll]);
    if Length(LColumn.DefaultValue) > 0 then
    begin
      case LProperty.PropertyType.TypeKind of
        tkString, tkWString, tkUString, tkWChar, tkLString, tkChar:
          LProperty.SetValue(Self, TValue.FromVariant(LValue).AsString);
        tkInteger, tkSet, tkInt64:
          LProperty.SetValue(Self, StrToIntDef(LValue, 0));
        tkFloat:
          begin
            if LProperty.PropertyType.Handle = TypeInfo(TDateTime) then // TDateTime
              LProperty.SetValue(Self, TValue.FromVariant(Date).AsType<TDateTime>)
            else
            if LProperty.PropertyType.Handle = TypeInfo(TDate) then // TDate
              LProperty.SetValue(Self, TValue.FromVariant(Date).AsType<TDate>)
            else
            if LProperty.PropertyType.Handle = TypeInfo(TTime) then// TTime
              LProperty.SetValue(Self, TValue.FromVariant(Time).AsType<TTime>)
            else
              LProperty.SetValue(Self, TValue.FromVariant(Time).AsType<Double>);
          end;
        tkRecord:
          LProperty.SetNullableValue(Self, LProperty.PropertyType.Handle, LValue);
        tkEnumeration:
          begin
            case LColumn.FieldType of
              ftString, ftFixedChar:
                LProperty.SetValue(Self, LProperty.GetEnumStringValue(Self, LValue));
              ftInteger:
                LProperty.SetValue(Self, LProperty.GetEnumIntegerValue(Self, LValue));
              ftBoolean:
                LProperty.SetValue(Self, TValue.FromVariant(LValue).AsBoolean);
            else
              raise Exception
                      .Create('Invalid type. Type enumerator supported [ftBoolena, ftInteger, ftFixedChar, ftString]');
            end;
          end;
      end;
    end;
  end;
end;

{ TORMBrObject }

constructor TORMBrObject.Create;
begin
  Self.SetDefaultValue;
end;

end.
