{
      ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi

                   Copyright (c) 2016, Isaque Pinheiro
                          All rights reserved.

                    GNU Lesser General Public License
                      Vers�o 3, 29 de junho de 2007

       Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
       A todos � permitido copiar e distribuir c�pias deste documento de
       licen�a, mas mud�-lo n�o � permitido.

       Esta vers�o da GNU Lesser General Public License incorpora
       os termos e condi��es da vers�o 3 da GNU General Public License
       Licen�a, complementado pelas permiss�es adicionais listadas no
       arquivo LICENSE na pasta principal.
}

{ @abstract(ORMBr Framework.)
  @created(20 Jul 2016)
  @author(Isaque Pinheiro <isaquepsp@gmail.com>)
  @author(Skype : ispinheiro)

  ORM Brasil � um ORM simples e descomplicado para quem utiliza Delphi.
}

unit ormbr.command.updater;

interface

uses
  DB,
  Rtti,
  Classes,
  SysUtils,
  Variants,
  TypInfo,
  Generics.Collections,
  ormbr.core.consts,
  ormbr.rtti.helper,
  ormbr.mapping.classes,
  ormbr.mapping.attributes,
  ormbr.command.abstract,
  ormbr.factory.interfaces,
  ormbr.types.database,
  ormbr.types.blob;

type
  TCommandUpdater = class(TDMLCommandAbstract)
  private
    function GetParamValue(AInstance: TObject; AProperty: TRttiProperty;
      AFieldType: TFieldType): Variant;
  public
    constructor Create(AConnection: IDBConnection; ADriverName: TDriverName;
      AObject: TObject); override;
    function GenerateUpdate(AObject: TObject;
      AModifiedFields: TList<string>): string;
  end;

implementation

uses
  ormbr.objects.helper;

{ TCommandUpdater }

constructor TCommandUpdater.Create(AConnection: IDBConnection;
  ADriverName: TDriverName; AObject: TObject);
var
  LColumn: TColumnMapping;
begin
  inherited Create(AConnection, ADriverName, AObject);
  for LColumn in AObject.GetPrimaryKey do
  begin
    with FParams.Add as TParam do
    begin
      Name := LColumn.ColumnName;
      DataType := LColumn.FieldType;
    end;
  end;
end;

function TCommandUpdater.GenerateUpdate(AObject: TObject;
  AModifiedFields: TList<string>): string;
var
  LFor: Integer;
  LRttiType: TRttiType;
  LProperty: TRttiProperty;
  LParams: TParams;
  LColumn: TColumnMapping;
  LColumnAtt: TCustomAttribute;
  LColumnName: string;
begin
  Result := '';
  FCommand := '';
  if AModifiedFields.Count = 0 then
    Exit;

  /// <summary>
  /// Variavel local � usado como par�metro para montar o script s� com os
  /// campos PrimaryKey.
  /// </summary>
  LParams := TParams.Create(nil);
  try
    for LColumn in AObject.GetPrimaryKey do
    begin
      if LColumn = nil then
        raise Exception.Create(cMESSAGEPKNOTFOUND);

      with LParams.Add as TParam do
      begin
        Name := LColumn.ColumnName;
        DataType := LColumn.FieldType;
        ParamType := ptUnknown;
        Value := LColumn.PropertyRtti.GetNullableValue(AObject).AsVariant;
      end;
    end;
    FCommand := FGeneratorCommand
                  .GeneratorUpdate(AObject, LParams, AModifiedFields);
    Result := FCommand;
    /// <summary>
    /// Gera todos os par�metros, sendo os campos alterados primeiro e o do
    /// PrimaryKey por �ltimo, usando LParams criado local.
    /// </summary>
    AObject.GetType(LRttiType);
    for LProperty in LRttiType.GetProperties do
    begin
      if LProperty.IsNoUpdate then
        Continue;
      LColumnAtt := LProperty.GetColumn;
      if LColumnAtt <> nil then
      begin
        LColumnName := Column(LColumnAtt).ColumnName;
        if AModifiedFields.IndexOf(LColumnName) > -1 then
        begin
          with LParams.Add as TParam do
          begin
            Name := LColumnName;
            DataType := Column(LColumnAtt).FieldType;
            ParamType := ptInput;
            Value := GetParamValue(AObject, LProperty, DataType);
          end;
        end;
      end;
    end;
    FParams.Clear;
    for LFor := LParams.Count -1 downto 0 do
    begin
      with FParams.Add as TParam do
      begin
        Name := LParams.Items[LFor].Name;
        DataType := LParams.Items[LFor].DataType;
        Value := LParams.Items[LFor].Value;
        ParamType := LParams.Items[LFor].ParamType;
      end;
    end;
  finally
    LParams.Free;
  end;
end;

function TCommandUpdater.GetParamValue(AInstance: TObject;
  AProperty: TRttiProperty; AFieldType: TFieldType): Variant;
begin
  if AProperty.IsNullValue(AInstance) then
  begin
    Result := Null;
    Exit;
  end;

  case AProperty.PropertyType.TypeKind of
    tkEnumeration:
      Result := AProperty.GetEnumToFieldValue(AInstance, AFieldType).AsType<Variant>;
    tkRecord:
      begin
        if AProperty.IsBlob then
          Result := AProperty.GetNullableValue(AInstance).AsType<TBlob>.ToBytes
        else
        if AProperty.IsNullable then
          Result := AProperty.GetNullableValue(AInstance).AsType<Variant>;
      end
  else
    Result := AProperty.GetValue(AInstance).AsType<Variant>;
  end;
end;

end.
